#include <iostream>
#include <vector>
#include <cmath>
#include <portaudio.h>
#include <fftw3.h>
#include <SFML/Graphics.hpp>

#define SAMPLE_RATE 44100
#define FRAMES_PER_BUFFER 1024
#define FFT_SIZE 1024
#define MAX_FREQ_INDEX (SAMPLE_RATE / 2 / (SAMPLE_RATE / FFT_SIZE)) // 22050 Hz
#define SPECTRUM_WIDTH 800
#define SPECTRUM_HEIGHT 600

typedef std::vector<float> Buffer;

class AudioHandler {
public:
    AudioHandler();
    ~AudioHandler();
    bool start();
    void stop();
    const Buffer& getBuffer() const { return buffer; }

private:
    static int audioCallback(const void* inputBuffer, void* outputBuffer,
                             unsigned long framesPerBuffer,
                             const PaStreamCallbackTimeInfo* timeInfo,
                             PaStreamCallbackFlags statusFlags,
                             void* userData);

    PaStream* stream;
    Buffer buffer;
};

AudioHandler::AudioHandler() : stream(nullptr) {
    buffer.resize(FFT_SIZE);
    Pa_Initialize();
}

AudioHandler::~AudioHandler() {
    Pa_Terminate();
}

bool AudioHandler::start() {
    Pa_OpenDefaultStream(&stream,
                         1, /* input channels */
                         0, /* output channels */
                         paFloat32, /* sample format */
                         SAMPLE_RATE,
                         FRAMES_PER_BUFFER,
                         audioCallback,
                         this);
    return Pa_StartStream(stream) == paNoError;
}

void AudioHandler::stop() {
    Pa_StopStream(stream);
    Pa_CloseStream(stream);
}

int AudioHandler::audioCallback(const void* inputBuffer, void* outputBuffer,
                                unsigned long framesPerBuffer,
                                const PaStreamCallbackTimeInfo* timeInfo,
                                PaStreamCallbackFlags statusFlags,
                                void* userData) {
    auto* handler = static_cast<AudioHandler*>(userData);
    const float* in = static_cast<const float*>(inputBuffer);
    for (unsigned long i = 0; i < framesPerBuffer; ++i) {
        handler->buffer[i] = in[i];
    }
    return paContinue;
}

void performFFT(const Buffer& buffer, std::vector<float>& outMagnitudes) {
    fftwf_complex* out = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * FFT_SIZE);
    fftwf_plan plan = fftwf_plan_dft_r2c_1d(FFT_SIZE, const_cast<float*>(buffer.data()), out, FFTW_ESTIMATE);

    fftwf_execute(plan);

    for (int i = 0; i < MAX_FREQ_INDEX; ++i) {
        outMagnitudes[i] = std::sqrt(out[i][0] * out[i][0] + out[i][1] * out[i][1]);
    }

    fftwf_destroy_plan(plan);
    fftwf_free(out);
}

void updateSpectrogram(std::vector<std::vector<float>>& spectrogram, const std::vector<float>& magnitudes) {
    for (size_t y = 0; y < spectrogram.size(); ++y) {
        for (size_t x = 1; x < spectrogram[y].size(); ++x) {
            spectrogram[y][x - 1] = spectrogram[y][x];
        }
        spectrogram[y][spectrogram[y].size() - 1] = magnitudes[y];
    }
}

void drawSpectrogram(const std::vector<std::vector<float>>& spectrogram, sf::Image& image) {
    for (size_t y = 0; y < spectrogram.size(); ++y) {
        for (size_t x = 0; x < spectrogram[y].size(); ++x) {
            float magnitude = spectrogram[y][x];
            sf::Uint8 color = static_cast<sf::Uint8>(std::min(255.0f, magnitude * 1000.0f));
            image.setPixel(x, spectrogram.size() - 1 - y, sf::Color(color, color, color));
        }
    }
}

int main() {
    AudioHandler audioHandler;
    if (!audioHandler.start()) {
        std::cerr << "Error starting audio stream" << std::endl;
        return -1;
    }

    sf::RenderWindow window(sf::VideoMode(SPECTRUM_WIDTH, SPECTRUM_HEIGHT), "Real-time Spectrogram");
    std::vector<float> magnitudes(MAX_FREQ_INDEX);
    std::vector<std::vector<float>> spectrogram(MAX_FREQ_INDEX, std::vector<float>(SPECTRUM_WIDTH, 0.0f));

    sf::Image image;
    image.create(SPECTRUM_WIDTH, MAX_FREQ_INDEX, sf::Color::Black);
    sf::Texture texture;
    sf::Sprite sprite;

    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
        }

        performFFT(audioHandler.getBuffer(), magnitudes);
        updateSpectrogram(spectrogram, magnitudes);
        drawSpectrogram(spectrogram, image);

        texture.loadFromImage(image);
        sprite.setTexture(texture);
        sprite.setScale(1.f, static_cast<float>(SPECTRUM_HEIGHT) / MAX_FREQ_INDEX); // Scale to window height

        window.clear();
        window.draw(sprite);
        window.display();
    }

    audioHandler.stop();
    return 0;
}
