# Real-time Audio Spectrogram

This project captures audio input from a microphone, performs an FFT, and displays a real-time spectrogram. The program is implemented in C++ and uses PortAudio for audio capture, FFTW for performing the FFT, and SFML for rendering the spectrogram.

## Dependencies

To build and run this project, you will need the following libraries installed on your system:

- **PortAudio**: For capturing audio input.
- **FFTW**: For performing the FFT.
- **SFML**: For rendering the spectrogram.

### Installing Dependencies on Ubuntu

Use the following commands to install the necessary libraries on Ubuntu:

```sh
sudo apt-get update
sudo apt-get install libportaudio2 libfftw3-dev libsfml-dev
```

## Building & Running

```bash
g++ -std=c++11 -o spectrogram main.cpp -lportaudio -lsfml-graphics -lsfml-window -lsfml-system -lfftw3f

./spectrogram
```

## Screenshot

![The screenshot](demo.png "The screenshot")